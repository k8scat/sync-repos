package main

import (
	"flag"
	"fmt"
	"log"
	"sync"

	"github.com/k8scat/repot/api/github"
)

var (
	orgName  = "bangwork"
	orgToken = "d9008d9bdef6fb314fdfe0d03bac5d6933953905"

	targetOrgName  = "projects-ones"
	targetOrgToken = "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"
	targetOrgID    = "MDEyOk9yZ2FuaXphdGlvbjg1MDA0NTEx"

	mirrorDir = "projects"
	backupDir = "clone"

	action string
)

func main() {
	flag.StringVar(&action, "a", "", "action type")
	flag.Parse()

	repos, err := listRepos()
	if err != nil {
		panic(err)
	}
	switch action {
	case "list":
		for _, repo := range repos {
			fmt.Println(repo.Name)
		}
	case "create":
		createRepos(repos)
	default:
		panic("supported actions: list and create")
	}
}

func createRepos(repos []*github.Repository) {
	client := github.NewClient(targetOrgToken)
	var wg sync.WaitGroup
	for _, repo := range repos {
		wg.Add(1)
		go func(wg *sync.WaitGroup, client *github.Client, repo *github.Repository) {
			defer wg.Done()
			err := client.CreateRepo(targetOrgID, repo.Name, repo.Description, github.RepositoryVisibilityPrivate)
			if err != nil {
				log.Printf("[%s] create repo failed: %v", repo.Name, err)
			}
		}(&wg, client, repo)
	}
	wg.Wait()
}

func listRepos() ([]*github.Repository, error) {
	allRepos := make([]*github.Repository, 0)
	var lastCursor string
	var repos []*github.Repository
	var err error
	for {
		org := github.NewClient(orgToken)
		repos, lastCursor, err = org.ListOrgRepos(orgName, 100, lastCursor)
		if err != nil {
			return nil, err
		}
		allRepos = append(allRepos, repos...)
		if lastCursor == "" {
			break
		}
	}
	return allRepos, nil
}
