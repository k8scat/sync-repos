package git

import (
	"os/exec"
)

const (
	CommandClone = "clone"
	CommandPush  = "push"
	CommandLFS   = "lfs"
	CommandFetch = "fetch"
)

func BareClone(repo, dir string) ([]byte, error) {
	return Exec("", CommandClone, repo, dir, "--bare")
}

func AllPush(workDir, repo string) ([]byte, error) {
	return Exec(workDir, CommandPush, "--all", repo)
}

func MirrorPush(workDir, repo string) ([]byte, error) {
	return Exec(workDir, CommandPush, "--mirror", repo)
}

func FetchLFS(workDir string) ([]byte, error) {
	return Exec(workDir, CommandLFS, "fetch", "--all")
}

func PushLFS(workDir, repo string) ([]byte, error) {
	return Exec(workDir, CommandLFS, "push", "--all", repo)
}

func Exec(workdir, command string, options ...string) ([]byte, error) {
	cmd := exec.Command("git")
	cmd.Args = append(cmd.Args, command)
	cmd.Args = append(cmd.Args, options...)
	cmd.Dir = workdir
	return cmd.Output()
}
