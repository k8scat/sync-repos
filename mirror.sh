#!/bin/bash
set -x

# export GIT_TRACE_PACKET=1
# export GIT_TRACE=1
# export GIT_CURL_VERBOSE=1

cd /home/hsowan/go/src/github.com/k8scat/repot

orgToken="d9008d9bdef6fb314fdfe0d03bac5d6933953905"
orgName="bangwork"

targetOrgToken="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"
targetOrgName="projects-ones"

mirror_dir="projects"
work_dir=$(pwd -P)

./bin/repot -a create
./bin/repot -a list | while read repo; do
    repo_dir="${mirror_dir}/${repo}"
    if [[ -d "${repo_dir}" ]]; then
        cd ${repo_dir}
        git fetch -p origin
    else
        git clone --bare https://${token}@github.com/${org}/${repo}.git ${repo_dir}
        cd ${repo_dir}
    fi

    repo_url="https://${targetOrgToken}@github.com/${targetOrgName}/${repo}.git"
    git push --all -f ${repo_url}
    git push --mirror -f ${repo_url}
    cd ${work_dir}
done
