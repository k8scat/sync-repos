package github

import (
	"encoding/json"
	"log"

	"github.com/tidwall/gjson"
)

type User struct {
	ID    string `json:"id"`
	Login string `json:"login"`
}

func (c *Client) GetViewer() (*User, error) {
	query := `
	{
		viewer {
			id
			login
		}
	}
	`
	raw, err := c.Query(query, nil)
	if err != nil {
		return nil, err
	}
	log.Println(raw)
	user := new(User)
	err = json.Unmarshal([]byte(gjson.Get(raw, "data.viewer").Raw), &user)
	return user, err
}

func (c *Client) GetUserID(username string) (string, error) {
	query := `
	query ($username: String!) {
		user (login: $username) {
			id
		}
	}
	`
	raw, err := c.Query(query, nil)
	if err != nil {
		return "", err
	}
	return gjson.Get(raw, "data.user.id").String(), err
}
