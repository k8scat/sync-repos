package github

import (
	"encoding/json"
	"errors"
	"log"
	"strings"

	"github.com/tidwall/gjson"
)

type RepositoryVisibility string

const (
	RepositoryAffiliationOwner = "owner"

	RepositoryVisibilityPrivate  RepositoryVisibility = "PRIVATE"
	RepositoryVisibilityPublic   RepositoryVisibility = "PUBLIC"
	RepositoryVisibilityInternal RepositoryVisibility = "INTERNAL"
)

// https://docs.github.com/en/graphql/reference/objects#repository
type Repository struct {
	ID          string      `json:"id"`
	Name        string      `json:"name"`
	Description string      `json:"description"`
	IsPrivate   bool        `json:"isPrivate"`
	IsEmpty     bool        `json:"isEmpty"`
	IsFork      bool        `json:"isFork"`
	Parent      *Repository `json:"parent"`
	Owner       *User       `json:"owner"`
	URL         string      `json:"url"`
}

func (c *Client) ListOrgRepos(org string, first int, after string) (repos []*Repository, lastCursor string, err error) {
	if org == "" {
		err = errors.New("Org cannot be empty")
		return
	}
	if first <= 0 {
		return
	}

	query := `
	query ($org: String!, $first: Int!, $after: String) {
		organization (login: $org) {
			repositories (first: $first, after: $after) {
				edges {
					node {
						name
						description
						isPrivate
						isFork
						parent {
							name
							owner {
								login
							}
						}
					}
					cursor
				}
				pageInfo {
					hasNextPage
				}
			}
		}
	}
	`
	variables := map[string]interface{}{
		"org":   org,
		"first": first,
	}
	if after != "" {
		variables["after"] = after
	}
	var raw string
	raw, err = c.Query(query, variables)
	if err != nil {
		return
	}
	edges := gjson.Get(raw, "data.organization.repositories.edges").Array()
	if len(edges) == 0 {
		return
	}

	repos = make([]*Repository, 0)
	for _, e := range edges {
		repo := new(Repository)
		if err = json.Unmarshal([]byte(gjson.Get(e.Raw, "node").Raw), &repo); err != nil {
			return
		}
		repos = append(repos, repo)
	}

	hasNextPage := gjson.Get(raw, "data.organization.repositories.pageInfo.hasNextPage").Bool()
	if hasNextPage {
		lastCursor = gjson.Get(edges[len(edges)-1].Raw, "cursor").Str
	}
	return
}

func (c *Client) ListUserRepos(username string, first int, after string) (repos []*Repository, hasNextPage bool, lastCursor string, err error) {
	if username == "" {
		err = errors.New("Username cannot be empty")
		return
	}
	if first <= 0 {
		return
	}

	query := `
	query ($username: String!, $first: Int!, $after: String) {
		user (login: $username) {
			repositories (first: $first, after: $after) {
				edges {
					node {
						name
						description
						isPrivate
						isFork
						parent {
							name
							owner {
								login
							}
						}
					}
					cursor
				}
				pageInfo {
					hasNextPage
				}
			}
		}
	}
	`
	variables := map[string]interface{}{
		"username": username,
		"first":    first,
	}
	if after != "" {
		variables["after"] = after
	}
	var raw string
	raw, err = c.Query(query, variables)
	if err != nil {
		return
	}
	edges := gjson.Get(raw, "data.organization.repositories.edges").Array()
	if len(edges) == 0 {
		return
	}

	repos = make([]*Repository, 0)
	for _, e := range edges {
		repo := new(Repository)
		if err = json.Unmarshal([]byte(gjson.Get(e.Raw, "node").Raw), &repo); err != nil {
			return
		}
		repos = append(repos, repo)
	}

	hasNextPage = gjson.Get(raw, "data.organization.repositories.pageInfo.hasNextPage").Bool()
	if hasNextPage {
		lastCursor = gjson.Get(edges[len(edges)-1].Raw, "cursor").Str
	}
	return
}

// The default repository owner is the token owner
// set the repository owner if ownerID is not empty
func (c *Client) CreateRepo(ownerID, name, description string, visibility RepositoryVisibility) error {
	mutation := `
	mutation ($name: String!, $description: String, $visibility: RepositoryVisibility!, $ownerId: ID) {
		createRepository (input: {
			name: $name,
			description: $description,
			visibility: $visibility,
			ownerId: $ownerId
		}) {
			repository {
				name
				description
            	isPrivate
				url
			}
		}
	}`
	variables := map[string]interface{}{
		"ownerId":     ownerID,
		"name":        name,
		"description": description,
		"visibility":  visibility,
	}
	raw, err := c.Query(mutation, variables)
	if err != nil {
		return err
	}
	if gjson.Get(raw, "errors").Exists() {
		if strings.Contains(gjson.Get(raw, "errors").String(), "Name already exists on this account") {
			log.Printf("[%s] repo existed", name)
			return nil
		}
		return errors.New(raw)
	}
	return nil
}
