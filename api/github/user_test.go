package github

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestGetViewer(t *testing.T) {
	setup()
	viewer, err := client.GetViewer()
	assert.Nil(t, err)
	fmt.Printf("viewer: %+v\n", viewer)
}
