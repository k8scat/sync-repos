package github

import "os"

var client *Client

func setup() {
	client = NewClient(os.Getenv("GITHUB_TOKEN"))
}
