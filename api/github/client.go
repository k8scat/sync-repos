package github

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
)

const Endpoint = "https://api.github.com/graphql"

type Client struct {
	Token string
}

func NewClient(token string) *Client {
	return &Client{
		Token: token,
	}
}

func (c *Client) Query(q string, v map[string]interface{}) (string, error) {
	htc := http.DefaultClient
	data := map[string]interface{}{
		"query": q,
	}
	if len(v) > 0 {
		data["variables"] = v
	}
	b := new(bytes.Buffer)
	if err := json.NewEncoder(b).Encode(data); err != nil {
		return "", err
	}
	req, err := http.NewRequest(http.MethodPost, Endpoint, b)
	if err != nil {
		return "", err
	}
	req.Header.Add("Authorization", fmt.Sprintf("bearer %s", c.Token))
	res, err := htc.Do(req)
	if err != nil {
		return "", err
	}
	return raw(res)
}

func raw(res *http.Response) (string, error) {
	defer res.Body.Close()
	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return "", err
	}
	if res.StatusCode != http.StatusOK {
		return "", fmt.Errorf("response: %+v, body: %s", res, string(b))
	}
	return string(b), nil
}
