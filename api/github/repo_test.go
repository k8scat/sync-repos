package github

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestListRepos(t *testing.T) {
	setup()
	repos, lastCursor, err := client.ListOrgRepos("bangwork", 50, "Y3Vyc29yOnYyOpHODXJGJQ==")
	assert.Nil(t, err)
	fmt.Printf("lastCursor: %s\n", lastCursor)
	fmt.Printf("repos len: %d\n", len(repos))
	for _, repo := range repos {
		fmt.Printf("%+v\n", repo)
	}
}
