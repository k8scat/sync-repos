package github

import (
	"encoding/json"

	"github.com/tidwall/gjson"
)

type Organization struct {
	ID string `json:"id"`
}

func (c *Client) GetOrganization(org string) (*Organization, error) {
	query := `
	query ($org: String!) {
		organization (login: $org) {
			id
		}
	}`
	variables := map[string]interface{}{
		"org": org,
	}
	raw, err := c.Query(query, variables)
	if err != nil {
		return nil, err
	}
	organization := new(Organization)
	err = json.Unmarshal([]byte(gjson.Get(raw, "data.organization").Raw), &organization)
	return organization, err
}
