#!/bin/bash
set -x

cd /home/hsowan/go/src/github.com/k8scat/repot

token="d9008d9bdef6fb314fdfe0d03bac5d6933953905"
org="bangwork"
backup_dir="clone"

work_dir=$(pwd -P)

# ./bin/reposync -l | xargs -I repo_name git clone https://${token}@github.com/${org}/repo_name.git clone/repo_name

./bin/repot -a list | while read repo; do
    repo_dir="${backup_dir}/${repo}"
    if [[ -d "${repo_dir}" ]]; then
        cd ${repo_dir}
        git fetch
        cd ${work_dir}
        continue
    fi

    git clone https://${token}@github.com/${org}/${repo}.git ${repo_dir}
done
