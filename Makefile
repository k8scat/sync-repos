NAME = repot

.PHYNO: build
build:
	go build -trimpath -o bin/repot main.go

.PHYNO: mirror
mirror:
	nohup ./mirror.sh > mirror.log 2>&1 &

.PHYNO: backup
backup:
	nohup ./backup.sh > backup.log 2>&1 &

